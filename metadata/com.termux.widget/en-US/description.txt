The Termux:Widget add-on provides a widget with shortcuts to run scripts placed in the $HOME/.shortcuts/ folder, allowing quick access to frequently used commands without typing.

NOTE: This is an add-on which requires that the main Termux app is installed to use.
.
