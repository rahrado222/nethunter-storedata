The Termux:Styling add-on provides color schemes and powerline-ready fonts to customize the appearance of the Termux terminal.

Long-press anywhere on the Termux terminal, select 'MORE...' and then 'Style' to use this add-on.

Want to suggest a new color scheme or font?
https://github.com/termux/termux-styling/issues/new
