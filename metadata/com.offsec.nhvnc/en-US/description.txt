A VNC (Virtual Network Computing) client: See and control your computer's desktop from your device. androidVNC lets you connect to most VNC servers: incl TightVNC, RealVNC on Win and Linux, x11vnc, and Apple Remote Desktop on OS/X. Lots of customizable features let you adapt the way your device controls map to the controls of your desktop.

This app is built and signed by Kali NetHunter.
 
